{ mkDerivation, base, bytestring, containers, deepseq, QuickCheck
, quickcheck-instances, stdenv, tasty, tasty-hunit
, tasty-quickcheck, text, transformers, unordered-containers
}:
mkDerivation {
  pname = "xmlbf";
  version = "0.5";
  src = ./.;
  libraryHaskellDepends = [
    base bytestring containers deepseq text transformers
    unordered-containers
  ];
  testHaskellDepends = [
    base bytestring deepseq QuickCheck quickcheck-instances tasty
    tasty-hunit tasty-quickcheck text transformers unordered-containers
  ];
  homepage = "https://gitlab.com/k0001/xmlbf";
  description = "XML back and forth! Parser, renderer, ToXml, FromXml, fixpoints";
  license = stdenv.lib.licenses.asl20;
}
